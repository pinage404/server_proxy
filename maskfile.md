# Commands

## test_request

```sh
watchexec --clear -- "sleep 1 ; curl --max-time 10 http://localhost:${PORT}"
```

## lint

```sh
FILES=$(shell git ls-files -- '*.py')
mypy ${FILES}
```

## format

```sh
FILES=$(shell git ls-files -- '*.py')
black ${FILES}
```

## run

```sh
./run.sh
```

## run_with_docker

```sh
docker compose up
```

## update

```sh
nix flake update
nix develop . --command \
	devbox update
nix develop . --command \
	poetry update
```
