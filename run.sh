#!/usr/bin/env sh
set -o errexit -o nounset

poetry install --only=main

exec \
    poetry run \
        python -m \
            flask run --host 0.0.0.0 --port $PORT
