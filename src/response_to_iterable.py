import collections
import itertools
import typing
import flask

Response: typing.TypeAlias = flask.Response | collections.abc.Iterable | str


def response_to_iterable(response: Response) -> collections.abc.Iterable[str] | bytes:
    if isinstance(response, flask.Response):
        return itertools.chain.from_iterable(response.response)

    if isinstance(response, collections.abc.Iterable):
        return itertools.chain.from_iterable(response)

    return bytes(response, encoding="utf-8")
