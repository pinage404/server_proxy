from flask import (
    Response,
    request,
)
from urllib.request import (
    Request,
    urlopen,
)
from urllib.error import HTTPError
from .sanitize import (
    format_request_data,
    sanitize_request_headers,
    sanitize_response_headers,
)


def pass_to_server(real_server_url: str):
    """
    must be the last decorator because decorated function will be ignored
    """

    def decorated(func):
        def wrapper(path=""):
            url = real_server_url + request.full_path

            request_headers = sanitize_request_headers(request.headers.items())

            request_data = format_request_data(request)

            api_request = Request(
                method=request.method,
                url=url,
                headers=request_headers,
                data=request_data,
            )

            try:
                with urlopen(api_request) as api_response:
                    response_headers = sanitize_response_headers(
                        api_response.getheaders()
                    )

                    response_data = api_response.read()

                    status = "%s %s" % (api_response.status, api_response.msg)

                    response = Response(
                        status=status,
                        headers=response_headers,
                        response=response_data,
                    )

                    return response

            except HTTPError as ex:
                response_headers = sanitize_response_headers(ex.headers.items())

                response_data = ""

                status = "%s %s" % (ex.code, ex.reason)

                response = Response(
                    status=status,
                    headers=response_headers,
                    response=response_data,
                )

                return response

        wrapper.__name__ = func.__name__

        return wrapper

    return decorated
