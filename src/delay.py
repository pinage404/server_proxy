from random import uniform as rand
import time
import typing

from .to_callable import CallableOfFloatOrFloat
from .to_callable import to_callable

DelayInSecond: typing.TypeAlias = CallableOfFloatOrFloat

# https://kenstechtips.com/index.php/download-speeds-2g-3g-and-4g-actual-meaning#Latency
LATENCY: typing.Dict[str, DelayInSecond] = {
    "2G": lambda: rand(10.0, 0.5),
    "3G": lambda: rand(0.5, 0.1),
    "4G": lambda: rand(0.1, 0.05),
    "5G": lambda: rand(0.05, 0.001),
}


def delay(seconds_to_wait: DelayInSecond = LATENCY["3G"]):
    seconds_to_wait = to_callable(seconds_to_wait)

    def decorated(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            time.sleep(seconds_to_wait())
            return result

        wrapper.__name__ = func.__name__

        return wrapper

    return decorated
