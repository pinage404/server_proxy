import typing
from flask import request


def cache(real_server_url: str):
    cache: typing.Dict[tuple[str, str, bytes], typing.Any] = {}

    def decorated(func):
        def wrapper(*args, **kwargs):
            url = real_server_url + request.full_path

            request_identifier = (request.method, url, request.data)

            if request_identifier not in cache:
                response = func(*args, **kwargs)
                cache.setdefault(request_identifier, response)

            response = cache[request_identifier]
            return response

        wrapper.__name__ = func.__name__

        return wrapper

    return decorated
